// Import the functions you need from the SDKs you need
import firebase from 'firebase/compat/app';
// import * as firebase from 'firebase'
import { GoogleAuthProvider } from "@firebase/auth";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_APIKEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTHDOMAN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECTID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGINGSENDERID,
  appId: process.env.REACT_APP_FIREBASE_APPID,
  measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENTID,
};

firebase.initializeApp(firebaseConfig);

const firebaseAuth = (provider) => firebase.auth().signInWithPopup(provider);

const database = firebase.database().ref();

export const authRef = firebase.auth();
export const loginGoogle = () => firebaseAuth(GoogleAuthProvider);
export const notesRef = database.child('notes');
export const tagsRef = database.child('tags');
export const pagesRef = database.child('pages');
export const usersRef = database.child('users');
export default firebase