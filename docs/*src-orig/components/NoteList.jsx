const NoteList = ({ notes, addNote, deleteNote, activeNote, setActiveNote }) => { // destructuring props

  const sortedNotes = notes.sort((a,b)=> b.lastModified - a.lastModified)

  return(
    <div className="box list">
      <div className="">
        <h1>📝 notes</h1>
        <button onClick={addNote}>Add</button>
      </div>

      <div className="list-notes">
        {sortedNotes.map((note) => (
          <div className={`list-note ${note.id === activeNote && "active"}`}
          onClick={() => setActiveNote(note.id)}
          >
          <div className="list-note-title">
            <strong>{note.title}</strong>
            <button onClick={()=>deleteNote(note.id)}>Delete</button>
          </div>
          <p>{note.body && note.body.substr(0, 100) + "..."}.</p>
          <small className="note-meta">{new Date(note.lastModified).toLocaleDateString("en-US", {
            day: "2-digit",
            month: "short",
            year: "numeric",
            hour12: true,
            hour: "numeric",
            minute: "2-digit",
          })}</small>
        </div>
        ))}

      </div>

    </div>
  )
}

export default NoteList;

// Notes
/*
- if there is a note body... render up to 100 chars
  - <p>{note.body && note.body.substr(0, 100) + "..."}.</p>

- toLocalDateString ... https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/toLocaleDateString
  - rad
  -       <small className="note-meta">{new Date(note.lastModified).toLocaleDateString("en-US", {
            day: "2-digit",
            month: "short",
            year: "numeric",
            hour12: true,
            hour: "numeric",
            minute: "2-digit",
          })}</small> ...etc

- bind to not run by default...
  - onClick={()=>deleteNote(note.id)}

- string builder (use backtics)
  - 

*/