Links and sources of code and inspiration  

## Inspiration
[stackedit.io](https://stackedit.io/)  
[excalidraw](https://excalidraw.com)  
[obsidian.md](https://obsidian.md)  
[vscode](https://code.visualstudio.com)  
[boostnote](https://boostnote.io)  
[apple notes](https://www.apple.com)  
[notion](https://www.notion.so)  

## Code & Guides
### Walkthroughs
intesnive program from: https://generalassemb.ly
James Grimshaw: https://youtu.be/ulOKYl5sHGk (used for initial site & wireframe tests)
https://www.youtube.com/watch?v=3qnrfkeguXg (serviceWorker base)
 - https://github.com/portexe/evernote-clone/blob/master/src/serviceWorker.js

  
### firebase
https://www.youtube.com/watch?v=zQyrwxMPm88 Fireship, react+firebase intro  
https://itnext.io/integrate-react-with-firebase-and-deploying-with-gitlab-netlify-8b47654c70bb)  
https://firebase.google.com/docs/web/setup  

## netifly frontend deployment
https://app.netlify.com/teams/juliajazz/overview 
pre-prod fe host https://app.netlify.com/sites/lucid-wescoff-80d099/overview  

  
## Styling
mia+oddbird https://www.oddbird.net/cascading-colors/docs/_  
css.irl https://css-irl.info/quick-and-easy-dark-mode-with-css-custom-properties/  
structure https://thoughtbot.com/blog/structure-for-styling-in-react-native
mui https://mui.com/getting-started/installation/

## Logo "Kata"
https://jisho.org/search/方%20%23kanji