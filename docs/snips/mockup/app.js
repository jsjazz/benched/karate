import './App.css';
import Sidebar from "./Sidebar";
import Main from "./Main";
import {useState, useEffect} from "react";
import { v4 as uuidv4 } from 'uuid'; // id randomizer! 🤙

function App() {
  // make state
  // const [notes, setNotes] = useState([]); // send to sidebar ->>
  // parse localstorage into useState
  // if new to the site, return empty array `|| []`
  const [notes, setNotes] = useState(
    localStorage.notes ? JSON.parse(localStorage.notes) : []
  );

  // ============================= STORE TO LOCAL STORAGE
  useEffect(() => {
    // store in local storage with each change
    localStorage.setItem("storedNotes", JSON.stringify(notes))
  }, [notes])


  // display active note state
  const [activeNote, setActiveNote] = useState(false)


  // ============================= ADD NOTE
  const addNote = () => {
    // make a note object
    const newNote = {
      id: uuidv4(),
      title: "",
      body: "",
      lastModified: Date.now(),
    };
    // append to current array with spread
    setNotes([newNote, ...notes]) // add newnote to existing notes
  }

  // ============================= EDIT NOTE
  const editNote = (editedNote) => {
    // use map to modify the array
    const editedNoteArr = notes.map((note) => {
      if(note.id === activeNote){
        // replace with edited note
        return editedNote
      }
      // otherwise return no edit
      return note
    })
    setNotes(editedNoteArr)
  }

  // ============================= DELETE NOTE
  const deleteNote = (idDelete) => {
    // filter fn -> loop through array, if true: keep, if fals: true
    setNotes(notes.filter((note) => note.id !== idDelete ))
  }

  // ============================= NOTE -> MAIN DISPLAY
  const getActiveNote = () => {
    return notes.find((note) => note.id === activeNote );
  }

  return (
    <div className="App">
      <h1>🙊</h1>
      <Sidebar 
        notes={notes} 
        addNote={addNote}
        deleteNote={deleteNote}
        activeNote={activeNote}
        setActiveNote={setActiveNote}
      />
      <Main activeNote={getActiveNote()} editNote={editNote} />
    </div>
  );
}

export default App;


// Notes
/*
- this SHOULD run automatically
<Main activeNote={getActiveNote()} />
*/