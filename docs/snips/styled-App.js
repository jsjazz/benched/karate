return (
  <div className="App">
    <CssBaseline/>
    <div className="wrapper">

      {/* HEADER */}
      <div className="box header">
        <h4>KarateNotes</h4>
      </div>  

      {/* MENU */}
      <div className="box menu">
        <img src={logo} width={50} alt='KarateNotes'/>
        <br/>
        <br/>
        <AccountCircleIcon fontSize="large" />
        <br/>
        <br/>
        <ListIcon fontSize="large" />
        <br/>
        <br/>
        <TagIcon fontSize="large" />
        <br/>
        <br/>
        <CollectionsBookmarkIcon fontSize="large" />

      </div>

      {/* LIST */}
      <div className="box list">
        <div className='list-controls'>
          <div>
            <SortIcon/>
            <SortByAlphaIcon/>
          </div>
        </div>
        <Divider />
        <div className='listed-note'>
          <div className='listed-note-title'>
            <h4>Note Title</h4>
          </div>
          <div className='listed-note-detail'>
            <Chip label="tag" size="small"/> <Chip label="tag2" size="small"/>
          </div>
          <small className='listed-note-meta'>
            Note Meta
          </small>
        </div>
        <Divider />
        <div className='listed-tag'>
          <div className='listed-tag-title'>
            Tag Title
          </div>
        </div>
      </div>

      {/* MAIN */}
      <div className="box main">
        <div className="main-header">
          <input type="text" id="title"/> <br/>
          <Divider />
          <div className='note-controls'>
            Note Controls or Tabs
          </div>
          <br/>
          <Divider />
          <div className='note-tabs'>
            Note Tabs or other options.
          </div>
          <br/>
          <Divider />
          <br/>
          {/* <textarea width='100%'
            display='block'
            resize='none'
            className="main-note" 
            id="body" 
            placeholder='kick your notes here...'
          /> */}
        <TextField
          id="outlined-multiline-static"
          label="Multiline"
          multiline
          rows={4}
          defaultValue="Default Value"
        />
        </div>
      </div>


    </div>
  </div>
);