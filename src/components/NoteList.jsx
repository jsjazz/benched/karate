import React from 'react'
import { Button, Card, } from 'react-bootstrap'

function NoteList({ notes, addNote, loading, activeNote, setActiveNote, deleteNote}) {
  // const sortedNotes = notes.sort((a,b)=> b.lastModified - a.lastModified)

  return (
    <div className='box list'>
      <div className="d-grid gap-2">
        <Button variant="dark" size="lg" onClick={() => addNote()}>
          Add Note
        </Button>  
      </div>
      {loading ? <h1>Loading...</h1> : null}
      <div className='notelist'>
        {notes.map((note) => (
          <Card className={`${note.uid === activeNote && "selected-note"}`} 
          onClick={() => setActiveNote(note.uid)} key={note.uid}>

            <Card.Header placeholder='Note Title'>
              {note.title}
              
            </Card.Header>

            <Card.Body>
              <p>{note.body && note.body.substr(0, 45) + "…"}.</p>
            </Card.Body>

            <Card.Subtitle className="mb-2 text-muted"> 

            <Card.Link style={{ color: 'black' }} onClick={() => deleteNote(note.uid)}>Delete</Card.Link>

            {new Date(note.lastUpdate).toLocaleDateString("en-US", {
              // weekday:"short",
              day: "2-digit",
              month: "2-digit",
              year: "2-digit",
              hour12: true,
              hour: "numeric",
              minute: "2-digit",
            })}
            </Card.Subtitle>
            
          </Card>
        ))}
      </div>
  </div>
  )
} 

export default NoteList


